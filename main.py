import requests
from bs4 import BeautifulSoup
import time
import subprocess
import logging


logging.basicConfig(level=logging.INFO)

def gestor(url):
    """
    Gestiona las requests al link de un archivo, Guardando su content en un archivo pdf
    """
    nombre = url.split('/')[-1]
    contenido = requests.get(url)

    if contenido.status_code == 200:
        with open('./oficiales/' + nombre, 'wb') as archivo:
            archivo.write(contenido.content)
            logging.info('archivo %s Creado' % nombre)
            logging.info('Desde la direccion %s' % url)


def buscar_url(url_ini):
    """   
    Funcion que busca dentro del contenido de una web los link dentros de un iframe
    """
    datos = requests.get('https://www.msp.gob.do/web/?page_id=6948#1586785071804-577a2da4-6f72')
    soup = BeautifulSoup(datos.content, 'html.parser')
    iframe = soup.iframe
    iframe_src = iframe.get('data-src')
    actual = 'https://www.msp.gob.do' + iframe_src
    yield actual
    for link in soup.find_all('a'):
        if 'especial' in link.get('href').lower():
            url = 'https://www.msp.gob.do'
            yield url + link.get('href')


descargado = 0
existente = 0

for x in buscar_url('https://www.msp.gob.do/web/?page_id=6948#1586785071804-577a2da4-6f72'):
    cont_carp = subprocess.run('ls',
                               shell=True,
                               capture_output=True,
                               text=True)
    
    if 'oficiales' not in cont_carp.stdout:
        subprocess.run('mkdir oficiales',
                       shell=True,
                       text=True,
                       check=True)
        
        logging.info('La carpeta de los archivos no se encuentra, creandola...')
    contenido_descargado = subprocess.run('ls ./oficiales/', shell=True,
                                          capture_output=True, text=True)
    nombre = x.split('/')[-1]
    if nombre not in contenido_descargado.stdout:
        gestor(x)
        descargado += 1
        time.sleep(0.5)
    else:
        existente += 1


logging.info(f'Tarea de descarga finalizada, {descargado} descargados, ya existian {existente}.')
